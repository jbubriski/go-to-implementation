using System;
using System.Linq;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Diagnostics;
using System.Resources;
using System.Reflection;
using System.Globalization;
using Microsoft.VisualStudio.TextManager.Interop;
using System.Runtime.InteropServices;

// Found a good sample implementation here: http://msdn.microsoft.com/en-us/library/90855k9f(v=VS.100).aspx

namespace GoToImplementation
{
    /// <summary>The object for implementing an Add-in.</summary>
    /// <seealso class='IDTExtensibility2' />
    public class Connect : Object, IDTExtensibility2, IDTCommandTarget
    {
        private DTE2 _applicationObject;
        private AddIn _addInInstance;

        /// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
        public Connect()
        {
        }

        /// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
        /// <param term='application'>Root object of the host application.</param>
        /// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
        /// <param term='addInInst'>Object representing this Add-in.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
        {
            _applicationObject = (DTE2)application;
            _addInInstance = (AddIn)addInInst;

            if (connectMode == ext_ConnectMode.ext_cm_UISetup)
            {
                Debug.WriteLine("Loading GoToImplementation Add In...");

                var contextGuids = new object[] { };
                var commands = (Commands2)_applicationObject.Commands;
                var commandBars = (CommandBars)_applicationObject.CommandBars;

                //var editorCommandBar = (CommandBar)commandBars["MenuBar"];
                //var editPopUp = (CommandBarPopup)editorCommandBar.Controls["Tools"];

                var editorCommandBar = (CommandBar)commandBars["Editor Context Menus"];
                var editPopUp = (CommandBarPopup)editorCommandBar.Controls["Code Window"];

                var command = commands.AddNamedCommand2(
                 _addInInstance
                 , "GoToImplementation"
                 , "Go To Implementation"
                 , "Goes to the first found implementation of an Interface."
                 , true
                 , Bitmap: Type.Missing
                 , ContextUIGUIDs: ref contextGuids
                 , vsCommandStatusValue: (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled
                 , CommandStyleFlags: (int)vsCommandStyle.vsCommandStylePictAndText
                 , ControlType: vsCommandControlType.vsCommandControlTypeButton);

                command.AddControl(editPopUp.CommandBar);

                Debug.WriteLine("Finished Loading GoToImplementation Add In.");
            }
        }

        /// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
        /// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
        {
        }

        /// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />		
        public void OnAddInsUpdate(ref Array custom)
        {
        }

        /// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnStartupComplete(ref Array custom)
        {
        }

        /// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
        /// <param term='custom'>Array of parameters that are host application specific.</param>
        /// <seealso class='IDTExtensibility2' />
        public void OnBeginShutdown(ref Array custom)
        {
        }

        public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
        {
            if (neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            {
                if (commandName == "GoToImplementation.Connect.GoToImplementation")
                {
                    status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
                    return;
                }
            }
        }

        public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
        {
            handled = false;
            if (executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault)
            {
                if (commandName == "GoToImplementation.Connect.GoToImplementation")
                {
                    handled = true;

                    var document = (EnvDTE.Document)_applicationObject.Documents.DTE.ActiveDocument;
                    var selection = (TextSelection)document.Selection;

                    var anchorPoint = selection.AnchorPoint;
                    var activePoint = selection.ActivePoint;

                    if (anchorPoint.DisplayColumn == activePoint.DisplayColumn)
                    {
                        selection.WordLeft();
                        selection.WordRight(true);
                    }
                    var typeName = selection.Text.Trim();

                    if (typeName.StartsWith("I"))
                    {
                        var typeFileName = string.Format(@"\{0}.cs", selection.Text.TrimStart('I'));

                        foreach (Project item in _applicationObject.Solution.Projects.OfType<Project>().Where(p => !p.Name.EndsWith(".Tests")))
                        {
                            foreach (ProjectItem projectItem in item.ProjectItems)
                            {
                                RecursiveProjectItemSearch(projectItem, typeFileName);

                            }
                        }
                    }

                    return;
                }
            }
        }

        private void RecursiveProjectItemSearch(ProjectItem projectItem, string typeFileName)
        {
            for (short i = 0; i < projectItem.FileCount; i++)
            {
                var fileName = projectItem.FileNames[i];

                if (fileName.EndsWith(typeFileName))
                {
                    _applicationObject.ItemOperations.OpenFile(fileName);
                }

                foreach(ProjectItem childProjectItem in projectItem.ProjectItems)
                {
                    RecursiveProjectItemSearch(childProjectItem, typeFileName);
                }
            }
        }
    }
}